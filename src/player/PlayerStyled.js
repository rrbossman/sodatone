import {
  Artist as ArtistIcon,
  Disc as DiscIcon,
} from './../icons/MetaIcons';
import Styled, { keyframes, css } from 'styled-components';

const SpinAnimation = keyframes`
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
`;

export const MetaInfoSpan = Styled.span`
  height: 45px;
  line-height: 45px;
  vertical-align: middle;
  float: left;
  color: #ffffff;
  font-size: 1.1rem;
`;

export const PlayerContainerDetails = Styled.div`
  display: flex;
  width: 100%;
  position: relative;
`;

export const PlayerContainer = Styled.div`
  width: 100%;
  min-height: 330px;
  padding: 20px 15px;
  position: relative;
  box-sizing: border-box;
`;

export const ControlsContainer = Styled.div`
  flex: 0 1 100%;
  margin-top: 25px;
  font-size: 1.5rem;
  position: fixed;
  width: 100%;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 15px 15px 5px 15px;
  background-color: #121212;
  box-shadow: 0 2px 8px 0 rgba(0,0,0,1);
`;

export const PlayerContainerInner = Styled.div`
  height: 100%;
  position: relative;
`;

export const PlayerContainerEmpty = Styled.div`
  width: auto;
  position: absolute;
  transform: translate(-50%, -50%);
  top: 50%;
  left: 50%;
`;

export const AlbumArtImageContainer = Styled.div`
  position: fixed;
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);
  height: 90%;
`;

export const PlayerContainerAttribute = Styled.div`
  padding: 15px;
  position: relative;
  flex: 1 1 45%;
  height: 75px;
`;

export const ProgressBarContainer = Styled.div`
  flex: 1 1 100%;
  display: flex;
  align-items: center;
`;

export const ProgressBarTime = Styled.div`
  flex: 0 0 70px;
  color: #ffffff;
  font-size: 1rem;
  text-align: center;
  vertical-align: middle;
  height: 30px;
  line-height: 30px;
`;

export const ProgressBarProgressContainer = Styled.div`
  display: block;
  height: 10px;
  flex: 1 1 100%;
  border-radius: 5px;
  background-color: rgba(255, 255, 255, 0.1);
  position: relative;
`;

export const ProgressBarProgress = Styled.div`
  height: 10px;
  width: ${({ percentWidth }) => percentWidth}%;
  background-color: #31bd38;
  border-radius: 5px;
  display: block;
  position: absolute;
  top: 0;
  left: 0;
`;

export const ProgressBarLoaded = Styled.div`
  height: 10px;
  width: ${({ percentWidth }) => percentWidth}%;
  background-color: rgba(255, 255, 255, 0.1);
  border-radius: 5px;
  display: block;
  position: absolute;
  top: 0;
  left: 0;
`;

export const AlbumArtImage = Styled.img`
  height: 100%;
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 1);
  border-radius: 50%;
  ${({ isPlaying }) => isPlaying ? css`animation: ${SpinAnimation} 4s linear infinite` : ''};
`;

export const PlayPauseButton = Styled.button`
  background-color: #101010;
  border: none;
  flex: 1 1 auto;
  height: 40px;
  padding: 10px;
  height: 135px;
  margin: -30px 20px;
  border-radius: 50%;
`;

export const ContainerRow = Styled.div`
  width: 100%;
  display: flex;
  align-items: top;
  position: relative;
  background-color: #101010;
`;

export const Artist = Styled(ArtistIcon)`
  margin-left: 9px;
  margin-right: 24px;
`;

export const Disc = Styled(DiscIcon)`
  margin-right: 15px;
  margin-left: 0;
`;

export const NextButton = Styled.button`
  border: none;
  margin: 12px 15px;
  flex: 0 0 51px;
  background-color: transparent;
`;

export const PreviousButton = Styled.button`
  border: none;
  margin: 12px 15px;
  flex: 0 0 51px;
  background-color: transparent;
`;
