import React, { Component } from 'react';
import ReactPlayer from 'react-player';
import {
  AlbumArtImage,
  AlbumArtImageContainer,
  Artist,
  Disc,
  PreviousButton,
  NextButton,
  ContainerRow,
  ControlsContainer,
  PlayPauseButton,
  PlayerContainer,
  PlayerContainerAttribute,
  PlayerContainerDetails,
  PlayerContainerEmpty,
  PlayerContainerInner,
  ProgressBarContainer,
  MetaInfoSpan,
  ProgressBarTime,
  ProgressBarProgressContainer,
  ProgressBarProgress,
  ProgressBarLoaded,
} from './PlayerStyled';
import {
  Pause,
  Play,
  Next,
  Previous,
} from './../icons/MediaIcons';
import Calculator from './../utils/Calculator';

class Player extends Component {
  state = {
    activeTrackIndex: 0,
    isPlaying: false,
    playedProgressPercent: 0,
    playedProgressTime: 0,
    playerBufferLoaded: 0,
  };

  playerElement = elm => { this.player = elm; }

  handleProgress = progress => {
    this.setState({
      playedProgressPercent: progress.played * 100,
      playedProgressTime: progress.playedSeconds * 1000,
      playerBufferLoaded: progress.loaded * 100,
    });
  }

  handleStartStopPlay = shouldPlay => {
    this.setState({ isPlaying: shouldPlay });
  }

  handleSkipTrack = direction => {
    const { activeTrackIndex } = this.state;
    const { tracks } = this.props;
    const nextTrackIndex = activeTrackIndex + direction;

    this.setState({
      activeTrackIndex: nextTrackIndex % tracks.length,
    });
  }

  handleSeekTrack = e => {
    const x = e.pageX - e.currentTarget.offsetLeft - 15;
    const width = e.currentTarget.offsetWidth;
    this.player.seekTo(x / width);
  }

  activeTrack = () => {
    const { tracks } = this.props;
    const { activeTrackIndex } = this.state;

    return tracks[activeTrackIndex];
  }

  render () {
    const {
      isPlaying,
      playedProgressPercent,
      playedProgressTime,
      playerBufferLoaded,
    } = this.state;

    const track = this.activeTrack();

    return (
      <PlayerContainer>
        <ReactPlayer
          ref={this.playerElement}
          playing={isPlaying}
          height="0"
          width="0"
          config={{ file: { forceAudio: true } }}
          progressInterval={100}
          url={track.mediaUrl}
          onReady={() => this.handleStartStopPlay(true)}
          onProgress={this.handleProgress}
          onEnded={() => this.handleSkipTrack(1)}
        />
        {
          track ?
            <PlayerContainerInner>
              <AlbumArtImageContainer>
                <AlbumArtImage isPlaying={isPlaying} src={ track.artworkUrl } />
              </AlbumArtImageContainer>
              <ControlsContainer>
                <ContainerRow>
                  <PreviousButton onClick={ () => this.handleSkipTrack(-1) }>
                    <Previous width={ 51 } height={ 51 } />
                    </PreviousButton>
                  <PlayerContainerDetails>
                    <PlayPauseButton onClick={ () => this.handleStartStopPlay(!isPlaying) }>
                      { !isPlaying && <Play width="auto" /> }
                      { isPlaying && <Pause width="auto" /> }
                    </PlayPauseButton>
                    <PlayerContainerAttribute>
                      <Disc width={ 30 } />
                      <MetaInfoSpan>{ track.trackName }</MetaInfoSpan>
                    </PlayerContainerAttribute>
                    <PlayerContainerAttribute>
                      <Artist width={ 30 } />
                      <MetaInfoSpan>{ track.artistName }</MetaInfoSpan>
                    </PlayerContainerAttribute>
                  </PlayerContainerDetails>
                  <NextButton onClick={ () => this.handleSkipTrack(1) }>
                    <Next width={ 51 } height={ 51 } />
                  </NextButton>
                </ContainerRow>
                <ContainerRow>
                  <ProgressBarContainer>
                    <ProgressBarTime>{ Calculator.secondsToTime(playedProgressTime) }</ProgressBarTime>
                    <ProgressBarProgressContainer onClick={this.handleSeekTrack}>
                      <ProgressBarProgress percentWidth={playedProgressPercent} />
                      <ProgressBarLoaded percentWidth={playerBufferLoaded} />
                    </ProgressBarProgressContainer>
                    <ProgressBarTime>
                      { Calculator.secondsToTime(track.durationMilliseconds) }
                    </ProgressBarTime>
                  </ProgressBarContainer>
                </ContainerRow>
              </ControlsContainer>
            </PlayerContainerInner>
          : <PlayerContainerEmpty>
              <span>No queued track!</span>
            </PlayerContainerEmpty>
        }
      </PlayerContainer>
    );
  }
}

export default Player;
