import React from 'react';

export const Play = ({
	fill = '#ffffff',
	width = '100%',
	height = '100%',
}) => (
	<svg
		version="1.1"
		xmlns="http://www.w3.org/2000/svg"
		x="0px"
		y="0px"
		viewBox="0 0 420 420"
		style={{
			enableBackground: 'new 0 0 420 420',
			width: width,
			height: height,
		}}
		xmlSpace="preserve">
		<g>
			<path fill={ fill } d="M210,21c104.216,0,189,84.784,189,189s-84.784,189-189,189S21,314.216,21,210S105.784,21,210,21 M210,0
				C94.031,0,0,94.024,0,210s94.031,210,210,210s210-94.024,210-210S325.969,0,210,0L210,0z"/>
			<path fill={ fill } d="M293.909,187.215l-111.818-73.591C162.792,100.926,147,109.445,147,132.545V287.42c0,23.1,15.813,31.647,35.147,18.998
				L293.86,233.31C313.187,220.647,313.208,199.913,293.909,187.215z M279.006,217.868l-99.295,64.981
				c-6.44,4.221-11.711,1.372-11.711-6.328V143.437c0-7.7,5.264-10.535,11.697-6.3l99.33,65.366
				C285.46,206.731,285.453,213.647,279.006,217.868z"/>
		</g>
	</svg>
);

export const Pause = ({
	fill = '#ffffff',
	width = '100%',
	height = '100%',
}) => (
	<svg
		version="1.1"
		xmlns="http://www.w3.org/2000/svg"
		x="0px"
		y="0px"
		viewBox="0 0 420 420"
		style={{
			enableBackground: 'new 0 0 420 420',
			width: width,
			height: height,
		}}
		xmlSpace="preserve">
		<g>
			<path fill={ fill } d="M210,21c104.216,0,189,84.784,189,189s-84.784,189-189,189S21,314.216,21,210S105.784,21,210,21 M210,0
				C94.031,0,0,94.024,0,210s94.031,210,210,210s210-94.024,210-210S325.969,0,210,0L210,0z"/>
			<g>
				<path fill={ fill } d="M259,108.941c-19.25,0-35,15.75-35,35v132.125c0,19.25,15.75,35,35,35s35-15.75,35-35V143.941
					C294,124.691,278.25,108.941,259,108.941z M273,276.066c0,7.7-6.3,14-14,14s-14-6.3-14-14V143.941c0-7.7,6.3-14,14-14
					s14,6.3,14,14V276.066z"/>
				<path fill={ fill } d="M161,108.941c-19.25,0-35,15.75-35,35v132.125c0,19.25,15.75,35,35,35s35-15.75,35-35V143.941
					C196,124.691,180.25,108.941,161,108.941z M175,276.066c0,7.7-6.3,14-14,14s-14-6.3-14-14V143.941c0-7.7,6.3-14,14-14
					s14,6.3,14,14V276.066z"/>
			</g>
		</g>
	</svg>
);

export const Next = ({
	fill = '#ffffff',
	width = '100%',
	height = '100%',
	className,
}) => (
  <svg
    className={ className }
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    x="0px"
    y="0px"
    viewBox='0 0 17.804 17.804'
    style={{
      enableBackground: '0 0 17.804 17.804',
      width: width,
      height: height,
    }}>
    <path fill={ fill } d='M2.44,0.043c0.14-0.071,0.304-0.051,0.425,0.044l10.777,8.502c0.094,0.074,0.152,0.191,0.152,0.312 c0,0.118-0.059,0.24-0.152,0.314L2.864,17.718c-0.072,0.056-0.162,0.086-0.25,0.086l-0.175-0.04 c-0.137-0.065-0.223-0.206-0.223-0.362V0.403C2.216,0.249,2.302,0.106,2.44,0.043z' />
    <path fill={ fill } d='M12.616,0.034h2.656c0.175,0,0.316,0.181,0.316,0.399v16.935c0,0.222-0.142,0.403-0.316,0.403h-2.656 c-0.174,0-0.316-0.182-0.316-0.403V0.434C12.3,0.215,12.443,0.034,12.616,0.034z' />
  </svg>
);

export const Previous = ({
	fill = '#ffffff',
	width = '100%',
	height = '100%',
	className,
}) => (
  <svg
    className={ className }
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    x="0px"
    y="0px"
    viewBox='0 0 17.802 17.802'
    style={{
      enableBackground: '0 0 17.802 17.802',
      width: width,
      height: height,
    }}>
      <path fill={ fill } d='M15.363,0.042c-0.139-0.07-0.303-0.051-0.424,0.043L4.163,8.587C4.069,8.664,4.01,8.78,4.01,8.9 c0,0.119,0.059,0.24,0.153,0.314l10.776,8.502c0.071,0.057,0.162,0.086,0.249,0.086l0.175-0.039 c0.139-0.064,0.225-0.207,0.225-0.361V0.403C15.588,0.249,15.502,0.107,15.363,0.042z' />
      <path fill={ fill } d='M5.188,0.033H2.53c-0.172,0-0.315,0.182-0.315,0.401V17.37c0,0.221,0.143,0.403,0.315,0.403h2.657 c0.174,0,0.315-0.183,0.315-0.403V0.434C5.503,0.215,5.361,0.033,5.188,0.033z' />
    </svg>
);
