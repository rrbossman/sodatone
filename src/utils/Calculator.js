class Calculator {
  static secondsToTime (seconds) {
    if(isNaN(seconds)) { return '0:00'; }
    seconds /= 1000;
    var sec = Math.floor(seconds % 60);
    return Math.floor(seconds / 60) + ':' + (sec < 10 ? '0' + sec : sec);
  }
}

export default Calculator;
