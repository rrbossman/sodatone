import React, { Component, Fragment } from 'react';
import Player from './player/Player.js';
import Tracks from './tracks/Tracks';
import { GlobalStyle } from './AppStyled';

class App extends Component {
  render() {
    return (
      <Fragment>
        <GlobalStyle />
        <Player tracks={Tracks} />
      </Fragment>
    );
  }
}

export default App;
